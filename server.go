package raft

import (
	"time"
	"math/rand"
)

const (
	// Follower - the server in the follower state
	Follower = iota
	// Candidate - the server in the candidate state
	Candidate
	// Leader - the server in the leader state
	Leader
)

// HeartbeatInterval is the interval in milliseconds that a leader sends out a heartbeat to the followers.
var HeartbeatInterval = time.Duration(250)

// HeartbeatTimeout is the time to wait in milliseconds for a heartbeat from the leader (should be a bit more than HeartbeatInterval)
var HeartbeatTimeout = time.Duration(HeartbeatInterval+50)

// The election timeout min and max values are used as the boundaries for random number generation for the
// election timeout for the current election. This means that each new election will have a different timeout,
// minimizing issues with race conditions between servers during an election.

// ElectionTimeoutMin is the minimum time in milliseconds that we spend counting votes. A timeout means a new election.
var ElectionTimeoutMin = int(100)

// ElectionTimeoutMax is the maximum time in milliseconds that we spend counting votes.
var ElectionTimeoutMax = int(300)

// Server represents the Raft server - whether acting as a leader, candidate, or follower
type Server struct {
	Send chan []byte   // Caller pushes to here to indicate a change to the model
	Recv chan []byte   // Caller pulls from here to get an element of the model
	Snap chan []byte   // Caller pulls from here to get a full snapshot of the model
	snap func() []byte // Caller function that returns the full snapshot of the model

	id        uint64
	peers     map[uint64]*peer

	// Note that these are all "fan-in" channels, meaning all our peers send to us on these four channels.
	logRq     chan *AppendEntriesRequest	// We receive log entries from our peers on this channel.
	logRs     chan *AppendEntriesResponse	// We receive log entry responses to our log entry requests sent to our peers on this channel.
	voteRq    chan *RequestVoteRequest		// We receive vote requests from our peers on this channel.
	voteRs    chan *RequestVoteResponse		// We receive vote responses to our vote requests sent to peers on this channel.

	term      uint64  // currentTerm: Current term
	votedFor  uint64 // votedFor:    Server candidate last voted for
	voteCount int    // the count of peers who voted for us as the next leader

	logEntries     []logEntry
	indexApplied   uint64 // lastApplied
	indexCommitted uint64 // commitIndex
	termCommitted  uint64 // not a distinct field in specification, but sent out in vote RPC

	tCand chan time.Time
}

type peer struct {
	serverID   uint64
	nextIndex  uint64
	matchIndex uint64
	logRq      chan *AppendEntriesRequest
	voteRq     chan *RequestVoteRequest
	voteRs     chan *RequestVoteResponse
}

type logEntry struct {
	term  uint64
	index uint64
	data  []byte
}

// NewServer creates a new server
func NewServer(snapshot func() []byte) *Server {
	s := &Server{
		Send:       make(chan []byte), // Caller pushes data changes here. Server propagates (or relays to leader).
		Recv:       make(chan []byte), // Raft sends data changes back to caller so it can update its model.
		Snap:       make(chan []byte), // Caller pulls model snapshot from here.
		snap:       snapshot,          // Caller function that can return the full model snapshot ready for sending to followers.
		peers:      make(map[serverID]*peer),
		logRq:      make(chan *AppendEntriesRequest),
		logRs:      make(chan *AppendEntriesResponse),
		voteRq:     make(chan *RequestVoteRequest),
		voteRs:     make(chan *RequestVoteResponse),
		logEntries: make([]logEntry, 100),
		tCand:      make(chan time.Time),
		id:         serverID(0),  // TODO - this needs to be the unique identifier for this server
	}
	return s
}

func newPeer() *peer {
	p := &peer{
		serverID: 0,
		nextIndex: 0,
		matchIndex: 0,
		logRq: make(chan *AppendEntriesRequest),
		voteRq: make(chan *RequestVoteRequest),
		voteRs: make(chan *RequestVoteResponse),
	}
	return p
}

func (s *Server) run() {
	var hbS <-chan time.Time
	var vtT <-chan time.Time
	hbR := time.After(HeartbeatInterval * time.Millisecond)

	// A server is either a leader, a follower, or a candidate.
	// Servers can continually change their role throughout their lifetime via "elections".
	// Every group of servers has exactly one leader.
	// All changes to data are send from the leader to the followers as "log entries".

	// A core principal is that we never know for sure if we are a leader.
	// We can *know* that we are a follower (or a candidate), and we can know that we are *not* a leader.
	// So we must be very careful with our "state" - the Raft specification describes all this.

	for {
		select {
		// If we're a leader, then we use this timer to tell us it's time to send another heartbeat.
		// If we're a follower, then this channel is nil, and we'll use the "hbR" channel below as the HB receive timeout.
		// We don't actually *know* if we're a leader. We could *think* we're a leader, but have been deposed!
		// So send out HBs and let our peers tell us if we've been ousted.
		case <-hbS:
			hbS = s.sendHeartbeat()
			// We'll listen for their responses on the aggregated (fan-in) peer response channel (s.logRs).
			
		// Only followers have this channel as non-nil. If we're a leader, then we send, we don't receive.
		// If this timer goes off, then we need to start a new election.
		case <-hbR:
			vtT = s.startVote()

		// Getting a "log request" is for one of two reasons:
		// 1) we're being asked to add this data as a log entry
		// 2) it has no data, and is simply a heartbeat (HB) from the leader
		// There are a number of things we might do, based on our own state.
		// We could get requests here, even if we think we're a leader. Must handle all cases.
		case lq := <-s.logRq:
			if lq.GetTerm() < s.term {
				// This log request is stale. Must reject it.
				s.logResponse(lq, false)

			} else if lq.GetTerm() >= s.term {
				// Current (or newer) term. Most likely normal LQ, either with data or just a HB.
				s.term = lq.GetTerm()

				// Could be that this is someone we were running against in a current election, and they won.
				if s.voteCount > 0 {
					// We were running in an election, and lost. Step down.
					s.voteCount = 0
					vtT = nil
				}

				// Check to see if we're in sync with the leader regarding committed entries.
				// Each LQ will have the last committed term/index; make sure we have the same term/index in our log.
				// *NOTE* : we might have the term/index entry, but not committed locally yet. This is normal. It means
				// the leader is telling us to commit our local entries up to that point.
				// If we don't have that latest term/index in our log (committed or not), then we have a gap. By rejecting
				// this LQ we'll get the leader to resend earlier and earlier entries, until we converge, at which point
				// the leader will catch us up by resending all we are missing.
				// This "sync-up" must take place on each LQ; both the LQs with new log entries and the LQs that are heartbeats.

				foundEntry := false
				for _, entry :=  range s.logEntries {
					if entry.term == lq.PrevLogTerm && entry.index == lq.PrevLogIndex {
						foundEntry = true
						break
					}
				}
				if foundEntry {
					// 
				}

				if len(lq.GetEntries()) > 0 {
					// Can we accept these new entries? Need to check for gaps (basically)
					// I need to have the lastTerm and lastIndex (sent in the request) in my log.
					// Otherwise, the leader has committed log entries that I don't have.
					// If I'm missing entries, reject the request, and the leader will start sending
					// me earlier entries until I can find a matching entry in my local log.

					
					if foundEntry {
						for _, entry := range lq.GetEntries() {
							s.Recv <-entry
						}
						s.logResponse(lq, true)
					} else {
						s.logResponse(lq, false)
					}
					
				}
				hbR = s.waitForHeartbeat()
			}

		// All peers send their responses to our log requests here.
		case ls := <-s.logRs:

		// All peers send their vote requests to us here.
		case vq := <-s.voteRq:
			// Is their term newer? That affects everything! If so, we must immediately become a follower, regardless of our current role and state.
			if vq.GetTerm() > s.term {
				s.term      = vq.GetTerm()
				s.voteCount = 0				// Since this is a new term, then we haven't yet voted in it (so clear any previous state for voting)
				s.votedFor  = 0
				//vtT = nil					// The vtT may already be nil. Only non-nil if we were in our own election and had to abandon.
				//hbR = s.waitForHeartbeat()	// A new term means *someone* better start sending us heartbeats!
			}

			// Okay, now let our state drive what we do (we could have reset it above, if this is a new term)
			// All we know is that someone wants our vote. Can we vote for them?
			if s.voteCount > 0 {
				// We're also running in this election (term). Can't vote for them. (Min value is 1 if we're running)
				s.voteResponse(vq, false)
			} else if s.votedFor == vq.GetCandidateId() {
				// I already said I'd vote for you. Send it again - network issue, dropped packet, whatever.
				s.voteResponse(vq, true)
			} else if s.votedFor != 0 {
				// We've already voted for someone (and it's not you). Sorry, can't vote for you.
				s.voteResponse(vq, false)
			} else if vq.GetLastLogTerm() > s.term {
				// Not running, and haven't voted. Their log is in a later term than ours. Automatically good.
				s.voteResponse(vq, true)
			} else if vq.GetLastLogTerm() < s.term {
				// Their log is still in a previous term. Automatically bad.
				s.voteResponse(vq, false)
			} else if vq.GetLastLogIndex() >= s.indexCommitted {
				// Same term, and their last index is same or better than ours. Good.
				s.votedFor = vq.GetCandidateId()
				s.voteResponse(vq, true)
			} else {
				// Same term, but within the term, our committed index is greater. They are behind. Bad.
				s.voteResponse(vq, false)
			}

		// All peers send their responses to our vote requests here.
		case vs := <-s.voteRs:
			if vs.GetTerm() == s.term {
				// You must be responding to the term I'm running in, otherwise I can't have your vote. (Stale term if I already lost)
				if s.voteCount > 0 {
					// If my voteCount is 0, then I already lost to someone else. So first make sure I'm still in the race. 
					if vs.GetVoteGranted() {
						// If you voted for me, bump up, and see if I won.
						// If you didn't vote for me, I'll eventually time out of the election if I don't get enough votes (tracking positive votes only)
						s.voteCount++
						if s.voteCount >= (len(s.peers)/2)+1 {
							vtT = nil  // Turn off the election timer. I've won!!

							// Could be I'm just becoming leader now, or I could already be the leader, and these are remaining responses.
							// If I'm just becoming leader, then do new leader stuff. Timers, etc.
							// If I've already won, there's nothing more I have to do. Everything is done.
							if hbS == nil {
								// Only leaders have this set, so I'm a *new* leader.
								hbS = s.sendHeartbeat()  // Sends a HB to proclaim our victory, and sets the timer for the next one.
								hbR = nil				 // I'm not looking for heartbeats, I send them. Clear this timer.
							}
						}
					}
				}
			}

		// Vote timeout. We started an election. Ran out of time waiting for votes. Need to start a new election.
		case <-vtT:
			vtT = s.startVote()
		}
	}
}

func (s *Server) startVote() (<-chan time.Time) {
	if ElectionTimeoutMin < 50 {
		ElectionTimeoutMin = 50
	}
	if ElectionTimeoutMax < 100 {
		ElectionTimeoutMax = 100
	}
	if ElectionTimeoutMax <= (ElectionTimeoutMin+50) {
		ElectionTimeoutMin = 50
		ElectionTimeoutMax = 100
	}
	gap := ElectionTimeoutMax - ElectionTimeoutMin
	rnd := rand.New(rand.NewSource(time.Now().UnixNano()))
	val := rnd.Intn(gap)

	s.term++
	s.voteCount = 1
	vr := &RequestVoteRequest{
		Term: uint64(s.term),
		CandidateId: uint64(s.id),
		LastLogIndex: uint64(s.indexCommitted),
		LastLogTerm: uint64(s.termCommitted),
	}
	for _, peer := range s.peers {
		peer.voteRq <-vr
	}
	return time.After(time.Duration(ElectionTimeoutMin+val) * time.Millisecond)
}

func (s *Server) voteResponse(vq *RequestVoteRequest, granted bool) {
	vs := &RequestVoteResponse{ Term: s.term, VoteGranted: granted }
	for _, peer := range s.peers {
		peer.voteRs <-vs
	}
}

func (s *Server) logResponse(lq *AppendEntriesRequest, approved bool) {

}

func (s *Server) sendHeartbeat() (<-chan time.Time) {
	for _, peer := range s.peers {
		peer.logRq <-&AppendEntriesRequest{}
	}
	return time.After(time.Duration(HeartbeatInterval) * time.Millisecond)
}

func (s *Server) waitForHeartbeat() (<-chan time.Time) {
	return time.After(time.Duration(HeartbeatTimeout) * time.Millisecond)
}
